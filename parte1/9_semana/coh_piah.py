import re
import math


def le_assinatura():
    # A funcao le os valores dos tracos linguisticos do modelo e devolve uma assinatura a ser comparada com os textos
    # fornecidos
    print("Bem-vindo ao detector automático de COH-PIAH.")

    wal = float(input("Entre o tamanho médio de palavra:"))
    ttr = float(input("Entre a relação Type-Token:"))
    hlr = float(input("Entre a Razão Hapax Legomana:"))
    sal = float(input("Entre o tamanho médio de sentença:"))
    sac = float(input("Entre a complexidade média da sentença:"))
    pal = float(input("Entre o tamanho medio de frase:"))

    return [wal, ttr, hlr, sal, sac, pal]


def le_textos():
    # A funcao le todos os textos a serem comparados e devolve uma lista contendo cada texto como um elemento
    i = 1
    textos = []
    texto = input("Digite o texto " + str(i) + " (aperte enter para sair):")
    while texto:
        textos.append(texto)
        i += 1
        texto = input("Digite o texto " + str(i) + " (aperte enter para sair):")

    return textos


def separa_sentencas(texto):
    # A funcao recebe um texto e devolve uma lista das sentencas dentro do texto
    sentencas = re.split(r'[.!?]+', texto)
    if sentencas[-1] == '':
        del sentencas[-1]
    return sentencas


def separa_frases(sentenca):
    # A funcao recebe uma sentenca e devolve uma lista das frases dentro da sentenca
    return re.split(r'[,:;]+', sentenca)


def separa_palavras(frase):
    # A funcao recebe uma frase e devolve uma lista das palavras dentro da frase
    return frase.split()


def n_palavras_unicas(lista_palavras):
    # Essa funcao recebe uma lista de palavras e devolve o numero de palavras que aparecem uma unica vez
    freq = dict()
    unicas = 0
    for palavra in lista_palavras:
        p = palavra.lower()
        if p in freq:
            if freq[p] == 1:
                unicas -= 1
            freq[p] += 1
        else:
            freq[p] = 1
            unicas += 1

    return unicas


def n_palavras_diferentes(lista_palavras):
    # Essa funcao recebe uma lista de palavras e devolve o numero de palavras diferentes utilizadas
    freq = dict()
    for palavra in lista_palavras:
        p = palavra.lower()
        if p in freq:
            freq[p] += 1
        else:
            freq[p] = 1

    return len(freq)


def calcula_tamanho_medio_palavras(palavras):
    return (somatorio_tamanho_palavras(palavras) / len(palavras))


def somatorio_tamanho_palavras(palavras):
    qtde_caracteres = 0

    for palavra in palavras:
        qtde_caracteres += len(palavra)
    return qtde_caracteres


def compara_assinatura(as_a, as_b):
    # IMPLEMENTAR. Essa funcao recebe duas assinaturas de texto e deve devolver o grau de similaridade nas assinaturas.
    somatorio = 0

    for i in range(0, 6):
        somatorio += math.fabs(as_a[i] - as_b[i])

    return somatorio / 6


def calcula_assinatura(texto):
    frases = []
    palavras = []

    qtde_caracteres_frase = []
    qtde_caracteres_sentenca = []
    sentencas = separa_sentencas(texto)

    for sentenca in sentencas:
        frases += separa_frases(sentenca)
        qtde_caracteres_sentenca.append(len(sentenca))

    for frase in frases:
        palavras += separa_palavras(frase)
        qtde_caracteres_frase.append(len(frase))

    wal = calcula_tamanho_medio_palavras(palavras)
    ttr = (n_palavras_diferentes(palavras) / len(palavras))
    hlr = (n_palavras_unicas(palavras) / len(palavras))
    sal = sum(qtde_caracteres_sentenca) / len(palavras)
    sac = (len(frases) / len(sentencas))
    pal = sum(qtde_caracteres_frase) / len(frases)

    return (wal, ttr, hlr, sal, sac, pal)


def avalia_textos(textos, ass_cp):
    # IMPLEMENTAR. Essa funcao recebe uma lista de textos e uma assinatura ass_cp e deve devolver o numero (1 a n) do
    # texto com maior probabilidade de ter sido infectado por COH-PIAH.
    similaridades = []

    for txto in textos:
        assinatura = calcula_assinatura(txto)
        similaridade = compara_assinatura(assinatura, ass_cp)
        similaridades.append(similaridade)

    menor_similaridade = min(similaridades)
    texto_infectado = (similaridades.index(menor_similaridade) + 1)
    # print("O autor do texto {} está infectado com COH-PIAH".format(texto_infectado))

    return texto_infectado


def le_arquivo(arquivo):
    f = open(arquivo, 'r')
    texto = f.read()
    f.close()
    return texto


def carrega_textos():
    textos = []
    # texto = le_arquivo('text0')
    # textos.append(texto)
    texto = le_arquivo('text1')
    textos.append(texto)
    texto = le_arquivo('text2')
    textos.append(texto)
    texto = le_arquivo('text3')
    textos.append(texto)
    return textos


def loadAssinaturas():
    return [5.571428571428571, 0.8253968253968254, 0.6984126984126984, 210.0, 4.5, 45.888888888888886]


def main():
    # ass = loadAssinaturas()
    # textos  = carrega_textos()
    ass = le_assinatura()
    textos = le_textos()
    print("O autor do texto {} esta infectado com COH-PIAH".format(avalia_textos(textos, ass)))


if __name__ == '__main__':
    main()
