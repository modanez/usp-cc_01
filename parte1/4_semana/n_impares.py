n = int(input('Digite o valor de n: '))

count = 1
impressoes = 0
is_final = True

while impressoes != n and is_final:
    if (count % 2) != 0:
        print(count)
        impressoes += 1
        if impressoes == n:
            is_final = False
    count += 1
