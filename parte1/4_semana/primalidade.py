n = int(input('Digite um número inteiro: '))

is_primo = True

if n == 2 or (n != 1 and n % 2 == 1):
    is_primo = True
else:
    is_primo = False

divisor = 3
while divisor < n and is_primo:
    if n % divisor == 0:
        is_primo = False
    divisor += 2

if is_primo:
    print('primo')
else:
    print('não primo')
