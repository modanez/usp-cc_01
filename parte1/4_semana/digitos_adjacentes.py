n = int(input('Digite um número inteiro: '))

is_adjacente = False
digito_anterior = n % 10
n = n // 10

while n > 0 and not is_adjacente:
    digito_atual = n % 10
    n = n // 10
    if digito_atual == digito_anterior:
        is_adjacente = True
    digito_anterior = digito_atual

if is_adjacente:
    print('sim')
else:
    print('não')
