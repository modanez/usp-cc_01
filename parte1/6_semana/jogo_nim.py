def computador_escolhe_jogada(n, m):
    # Devolve o numero de peças que foram removidas na jogada
    valor = 1

    while valor != m:
        if (n - valor) % (m + 1) == 0:
            return valor
        else:
            valor += 1
    return valor


def usuario_escolhe_jogada(n, m):
    valor = int(input('\nQuantas peças você vai tirar? '))
    while valor > m or valor <= 0 or valor > n:
        print('Oops! Jogada inválida! Tente de novo.')
        valor = int(input('\nQuantas peças você vai tirar? '))

    return valor


def partida():
    n = int(input('Quantas peças? '))
    m = int(input('Limite de peças por jogada? '))

    # while m < 1:
    #     print('A quantidade de peças por jogadas devem ser menor ou igual as peças totais')
    #     m = int(input('Limite de peças por jogada? '))

    is_usuario = True

    if n % (m + 1) == 0:
        print('\nVoce começa!')
    else:
        print('\nComputador começa!')
        is_usuario = False

    while n > 0:
        if is_usuario:
            valor = usuario_escolhe_jogada(n, m)
            n -= valor
            if valor == 1:
                print('\nVocê tirou uma peça')
            else:
                print('\nVocê tirou', valor, 'peças')
            is_usuario = False
        else:
            valor = computador_escolhe_jogada(n, m)
            n -= valor
            if valor == 1:
                print('\nO computador tirou uma peça')
            else:
                print('\nO computador tirou', valor, 'peças')

            is_usuario = True

        if n == 1:
            print('Agora resta apenas uma peça no tabuleiro.')
        else:
            if n != 0:
                print('Agora restam,', n, 'peças no tabuleiro.')

    print('Fim do jogo! O computador ganhou!')


def campeonato():
    qtde_partidas = 1
    placar_computador, placar_usuario = 0, 0

    while qtde_partidas < 4:
        print('**** Rodada', qtde_partidas, '****\n')
        if partida() == 1:
            placar_usuario += 1
        else:
            placar_computador += 1
        qtde_partidas += 1

    print('**** Final do campeonato! ****\n')
    print('Placar: Você', placar_usuario, 'X', placar_computador, 'Computador')


def main():
    print('Bem-vindo ao jogo do NIM! Escolha:')
    print('1 - para jogar uma partida isolada')
    print('2 - para jogar um campeonato 2')

    opcao = int(input('Escolha: '))

    while opcao != 1 and opcao != 2:
        print("Escolha uma opção válida!")
        opcao = int(input("Escolha: "))

    if opcao == 1:
        print('\nVoce escolheu uma partida!\n')
        partida()
    elif opcao == 2:
        print('\nVocê escolheu um campeonato!\n')
        campeonato()


if __name__ == '__main__':
    main()
