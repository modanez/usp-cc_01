def calcula_hipotenusa(a, b):
    # Para encontrar o quadrado de um número, basta multiplicá-lo por si mesmo, de modo que a2 = a × a.
    hipotenusa = ((a * a) + (b * b))
    return hipotenusa


def soma_hipotenusas(n):
    c = 1
    soma = 0
    while (c <= n):
        # Para encontrar o quadrado de um número, basta multiplicá-lo por si mesmo, de modo que a2 = a × a.
        c2 = (c * c)
        a = 1
        b = 1
        while (a < n):
            while (b < n):
                if (c2 == calcula_hipotenusa(a, b)):
                    print(c, end=' ')
                    soma = soma + c
                    a = n
                    break
                b += 1
            a += 1
            # reinicia o valor de tendo como base o valor de a antes de uma nova busca pelo n válido
            b = a
        c += 1
    return soma


x = int(input("Digite um número inteiro e positivo: "))

print('\n'+ str(soma_hipotenusas(x)))