def is_primo(n):
    fator = 2
    if n == 2:
        return True

    while n % fator != 0 and fator <= n / 2:
        fator += 1

    if n % fator == 0:
        return False
    else:
        return True


def n_primos(n):
    primos = 0
    if n == 1:
        return primos

    while n >= 2:
        if is_primo(n):
            primos += 1
        n -= 1
    return primos


def main():
    n = int(input("Informe um número inteiro: "))
    print(n_primos(n))


if __name__ == '__main__': main()
