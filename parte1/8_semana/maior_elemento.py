def maior_elemento(lista):
    # lista.sort()
    # return lista[-1]
    maior = lista[0]
    for item in lista:
        if item > maior:
            maior = item
    return maior


def main():
    lista = [1]
    print(maior_elemento(lista))


if __name__ == '__main__':
    main()