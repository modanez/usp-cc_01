def remove_repetidos(lista):
    aux = []
    for item in lista:
        if item not in aux:
            aux.append(item)
    aux.sort()
    return aux


def main():
    lista = [7,3,33,12,3,3,3,7,12,100]
    print(remove_repetidos(lista))


if __name__ == '__main__':
    main()