entrada = input('Por favor, entre com o número de segundos que deseja converter: ')

segundos = int(entrada)
dias = segundos // 86400
horas = segundos // 3600
resto_segundos = segundos % 3600
minutos = resto_segundos // 60
resto_total_segundos = resto_segundos % 60

if (horas >= 24): horas = horas % 24

print(dias, 'dias,', horas, 'horas,', minutos, 'minutos e', resto_total_segundos, 'segundos.')