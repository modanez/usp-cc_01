from math import sqrt

x1 = int(input('Insira o valor de x1: '))
y1 = int(input('Insira o valor de y1: '))
x2 = int(input('Insira o valor de x2: '))
y2 = int(input('Insira o valor de y2: '))

distancia = sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2))

if distancia >= 10:
    print('longe')
else:
    print('perto')
