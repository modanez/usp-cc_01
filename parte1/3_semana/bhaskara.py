from math import sqrt

a = int(input('Informe o valor de a: '))
b = int(input('Informe o valor de b: '))
c = int(input('Informe o valor de c: '))

delta = pow(b, 2) - 4 * a * c

if delta == 0:
    raiz1 = (-b + sqrt(delta)) / (2 * a)
    print('a raiz desta equação é', str(raiz1))
elif delta < 0:
    print('esta equação não possui raízes reais')
else:
    raiz1 = (-b + sqrt(delta)) / (2 * a)
    raiz2 = (-b - sqrt(delta)) / (2 * a)
    if raiz1 < raiz2:
        print('as raízes da equação são', str(raiz1), 'e', str(raiz2))
    else:
        print('as raízes da equação são', str(raiz2), 'e', str(raiz1))
