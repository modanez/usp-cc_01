def vogal(letra):
    if type(letra) != str:
        return None
    is_vogal = True
    letra = str(letra).lower()
    if letra == 'a' or letra == 'e' or letra == 'i' or letra == 'o' or letra == 'u':
        return is_vogal
    else:
        return not is_vogal
