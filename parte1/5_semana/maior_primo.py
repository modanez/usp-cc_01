def maior_primo(n):
    from math import sqrt
    # if n < 2:
    #     return None
    while n > 1:
        aux = sqrt(n)
        aux = int(aux)
        aux1 = 0
        while aux >= 2:
            if n % aux == 0:
                aux1 += 1
            if aux == 2:
                if aux1 == 0:
                    return n
            aux -= 1
        n -= 1
